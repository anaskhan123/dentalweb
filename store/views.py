from django.shortcuts import redirect, render
from django.http import HttpResponse
from .models.product import Products
from .models.catagory import Catagory
from django.views import View

# Create your views here.
class Home(View):

    def post(self, request):
        product = request.POST.get('product')
        cart = request.session.POST.get('cart')
        if cart:
            quantity = cart.get(product)
            if quantity:
               cart[product] = quantity+1
            else:
                cart[product] = 1
        else:
            cart = {}
            cart[product] = 1
            request.session['cart'] = cart
        return redirect("home")



    def get(self, request):
        products = None
        catagories = Catagory.get_all_catagories()
        catagoryID = request.GET.get('catagory')
        if catagoryID:
           products = Products.get_all_products_by_catagoryid(catagoryID)
        else:
           products = Products.get_all_products()
        data = {}
        data['catagories'] = catagories
        data['products'] = products
        return render(request, 'home.html', data)

   
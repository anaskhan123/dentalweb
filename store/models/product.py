from django.db import models
from .catagory import Catagory

class Products(models.Model):
    name = models.CharField(max_length=50)
    catagory = models.ForeignKey(Catagory, on_delete=models.CASCADE, default=1)
    price = models.IntegerField(default=0)
    description = models.CharField(max_length=200, default='')
    image = models.ImageField(upload_to='products/')

    @staticmethod
    def get_all_products():
        return Products.objects.all()

    @staticmethod
    def get_all_products_by_catagoryid(catagory_id):
        if catagory_id:
            return Products.objects.filter(catagory = catagory_id)
        else:
            return Products.get_all_products()
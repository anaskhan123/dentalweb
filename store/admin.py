from django.contrib import admin
from .models.product import Products
from .models.catagory import Catagory

class AdminProduct(admin.ModelAdmin):
    list_display = ['name', 'price', 'catagory']

class AdminCatagory(admin.ModelAdmin):
    list_display = ['name']

# Register your models here.
admin.site.register(Products,AdminProduct)
admin.site.register(Catagory,AdminCatagory)
